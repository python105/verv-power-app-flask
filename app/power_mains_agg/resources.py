from datetime import datetime
from flask_restplus import Resource, reqparse
from marshmallow import ValidationError
from app import db
from . import api
from .models import PowerMainsAgg
from .schemas import PowerMainsAggSchema

SCHEMA = PowerMainsAggSchema()

class PowerMainsAggResource(Resource):
    ''' Power resource for PowerMainsAgg model object '''

    def get(self, object_id=None):
        ''' returns a single PowerMainsAgg object with provided ID or a collection with provided start_time and end_time '''
        if object_id:
            power_item = PowerMainsAgg.query.filter_by(id=object_id).first()
            if not power_item:
                return {'status': 'failure', 'message': 'Item does not exist'}, 404
            data = SCHEMA.dump(power_item)
            return {'status': 'success', 'data': data}, 200
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('start_time', required=True, type=int)
            parser.add_argument('end_time', required=True, type=int)
            parser.add_argument('aggregation', required=True, choices=('none', 'hour', 'day', 'week', 'month', 'year'))
            args = parser.parse_args()

            switch = {
                "none": PowerMainsAgg.timestamp,
                "hour": PowerMainsAgg.get_datetime_by_hour,
                "day": PowerMainsAgg.get_datetime_by_day,
                "week": PowerMainsAgg.get_datetime_by_week,
                "month": PowerMainsAgg.get_datetime_by_month,
                "year": PowerMainsAgg.get_datetime_by_year,
            }

            time_format = switch.get(args['aggregation'])

            start_time = datetime.utcfromtimestamp(args['start_time'])
            end_time = datetime.utcfromtimestamp(args['end_time'])

            power_items = db.session.query(
                db.func.avg(PowerMainsAgg.current).label('current'), #avg rms A
                db.func.avg(PowerMainsAgg.voltage).label('voltage'), #avg - rms V
                db.func.avg(PowerMainsAgg.real_power).label('real_power'), #avg v * i W
                db.func.avg(PowerMainsAgg.apparent_power).label('apparent_power'), #avg Vrms * Irms VA
                db.func.avg(PowerMainsAgg.reactive_power).label('reactive_power'), #avg Vrms * Irms VA
                db.func.avg(PowerMainsAgg.power_factor).label('power_factor'), #avg power factor W/VA
                db.func.sum(PowerMainsAgg.cost).label('cost'), #sum of cost
                db.func.sum(PowerMainsAgg.standing_charge).label('standing_charge'), #sum of standing charge
                db.func.sum(PowerMainsAgg.energy).label('energy'), #sum of energy KWh
                db.func.sum(PowerMainsAgg.carbon).label('carbon'), #sum of carbon
                db.func.avg(PowerMainsAgg.frequency).label('frequency'), #avg frequency H
                db.func.avg(PowerMainsAgg.thdc).label('thdc'), #avg Total harmonic distortion current
                db.func.avg(PowerMainsAgg.current_harmonic_1).label('current_harmonic_1'), #avg energy in the 1st harmonic
                db.func.avg(PowerMainsAgg.current_harmonic_2).label('current_harmonic_2'), #avg energy in the 2nd harmonic
                db.func.avg(PowerMainsAgg.current_harmonic_3).label('current_harmonic_3'), #avg energy in the 3rd harmonic
                db.func.avg(PowerMainsAgg.thdv).label('thdv'), #avg Total harmonic distortion voltage
                db.func.avg(PowerMainsAgg.voltage_harmonic_1).label('voltage_harmonic_1'), #avg energy in the 1st harmonic
                db.func.avg(PowerMainsAgg.voltage_harmonic_2).label('voltage_harmonic_2'), #avg energy in the 2nd harmonic
                db.func.avg(PowerMainsAgg.voltage_harmonic_3).label('voltage_harmonic_3'), #avg energy in the 3rd harmonic
                PowerMainsAgg.channel,
                PowerMainsAgg.tz_offset,
                PowerMainsAgg.start_timestamp,
                PowerMainsAgg.end_timestamp,
                time_format.label('aggregate_time'),
            ).filter(PowerMainsAgg.timestamp.between(start_time, end_time)).group_by(time_format, PowerMainsAgg.channel).order_by(PowerMainsAgg.timestamp).all()
            data = SCHEMA.dump(power_items, many=True)
            return {'status': 'success', 'data': data}, 200

    def post(self):
        ''' creates PowerMainsAgg object and retuns created PowerMainsAgg object '''
        try:
            power = SCHEMA.load(api.payload)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.add(power)
        db.session.commit()
        data = SCHEMA.dump(power)
        return {'status': 'success', 'data': data}, 201

    def put(self, object_id):
        ''' update and existing PowerMainsAgg object with provided ID '''
        power_item = PowerMainsAgg.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        try:
            SCHEMA.load(api.payload, instance=power_item, session=db.session, partial=True)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204

    def delete(self, object_id):
        ''' deletes a single PowerMainsAgg object '''
        power_item = PowerMainsAgg.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        db.session.delete(power_item)
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204
