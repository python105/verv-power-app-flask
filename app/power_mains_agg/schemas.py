from app import ma
from .models import PowerMainsAgg

class PowerMainsAggSchema(ma.ModelSchema):
    ''' PowerMainsAgg Marshmallow schema Class '''
    class Meta:
        additional = ('aggregate_time',)
        dump_only = ('id', 'timestamp', 'tz_offset', )
        model = PowerMainsAgg
        strict = True
