from flask import Blueprint
from flask_restplus import Api

power_mains_agg = Blueprint('power_mains_agg', __name__)
api = Api(power_mains_agg)

from . import urls
from .schedulers import PowerMainsAggScheduler
PowerMainsAggScheduler()

# ERROR HANDLING
@api.errorhandler
def default_error_handler(error):
    return {'status': 'failure', "message": str(error)}
