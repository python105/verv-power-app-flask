from . import api
from .resources import PowerMainsAggResource

api.add_resource(PowerMainsAggResource, '/<int:object_id>', '/')
