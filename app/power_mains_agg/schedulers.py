from datetime import datetime, timedelta
from marshmallow import ValidationError
from apscheduler.schedulers.background import BackgroundScheduler
from app import db
from app.power_mains_raw.models import PowerMainsRaw
from .schemas import PowerMainsAggSchema

SCHEMA = PowerMainsAggSchema()

class PowerMainsAggScheduler(object):
    ''' Scheduler class for aggregating power data from raw table to agg table '''

    def __init__(self):
        self.timer = 15
        self._scheduler = BackgroundScheduler()
        self.job = self._scheduler.add_job(self.aggregate_task, 'cron', minute=('*/%i' % self.timer))
        self._scheduler.start()

    def aggregate_task(self):
        ''' aggregate the data using mathematical functions '''
        print('start')
        end_time = datetime.now()
        start_time = datetime.now() - timedelta(minutes=self.timer)
        power_items = db.session.query(
            db.func.avg(PowerMainsRaw.current).label('current'), #avg rms A
            db.func.avg(PowerMainsRaw.voltage).label('voltage'), #avg - rms V
            db.func.avg(PowerMainsRaw.real_power).label('real_power'), #avg v * i W
            db.func.avg(PowerMainsRaw.apparent_power).label('apparent_power'), #avg Vrms * Irms VA
            db.func.avg(PowerMainsRaw.reactive_power).label('reactive_power'), #avg Vrms * Irms VA
            db.func.avg(PowerMainsRaw.power_factor).label('power_factor'), #avg power factor W/VA
            db.func.sum(PowerMainsRaw.cost).label('cost'), #sum of cost
            db.func.sum(PowerMainsRaw.standing_charge).label('standing_charge'), #sum of standing charge
            db.func.sum(PowerMainsRaw.energy).label('energy'), #sum of energy KWh
            db.func.sum(PowerMainsRaw.carbon).label('carbon'), #sum of carbon
            db.func.avg(PowerMainsRaw.frequency).label('frequency'), #avg frequency H
            db.func.avg(PowerMainsRaw.thdc).label('thdc'), #avg Total harmonic distortion current
            db.func.avg(PowerMainsRaw.current_harmonic_1).label('current_harmonic_1'), #avg energy in the 1st harmonic
            db.func.avg(PowerMainsRaw.current_harmonic_2).label('current_harmonic_2'), #avg energy in the 2nd harmonic
            db.func.avg(PowerMainsRaw.current_harmonic_3).label('current_harmonic_3'), #avg energy in the 3rd harmonic
            db.func.avg(PowerMainsRaw.thdv).label('thdv'), #avg Total harmonic distortion voltage
            db.func.avg(PowerMainsRaw.voltage_harmonic_1).label('voltage_harmonic_1'), #avg energy in the 1st harmonic
            db.func.avg(PowerMainsRaw.voltage_harmonic_2).label('voltage_harmonic_2'), #avg energy in the 2nd harmonic
            db.func.avg(PowerMainsRaw.voltage_harmonic_3).label('voltage_harmonic_3'), #avg energy in the 3rd harmonic
            PowerMainsRaw.channel,
        ).filter(PowerMainsRaw.timestamp.between(start_time, end_time)).group_by(PowerMainsRaw.channel).all()
        if power_items:
            for power_item in power_items:
                try:
                    power_item_dict = power_item._asdict()
                    power_item_dict['start_timestamp'] = str(start_time)
                    power_item_dict['end_timestamp'] = str(end_time)
                    power = SCHEMA.load(power_item_dict)
                except ValidationError as err:
                    print(err)
                    return {'status': 'failure', 'message': err.messages}, 400
                db.session.add(power)
            db.session.commit()
            return {'status': 'success'}, 201
