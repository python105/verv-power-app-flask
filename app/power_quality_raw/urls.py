from . import api
from .resources import PowerQualityRawResource

api.add_resource(PowerQualityRawResource, '/<int:object_id>', '/')
