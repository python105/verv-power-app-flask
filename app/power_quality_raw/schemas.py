from app import ma
from .models import PowerQualityRaw


class PowerQualityRawSchema(ma.ModelSchema):
    ''' PowerQualityRaw Marshmallow schema Class '''
    class Meta:
        additional = ('aggregate_time',)
        dump_only = ('id', 'timestamp', 'tz_offset', )
        model = PowerQualityRaw
        strict = True
