from flask import Blueprint
from flask_restplus import Api

power_quality_raw = Blueprint('power_quality_raw', __name__)
api = Api(power_quality_raw)

from . import urls
