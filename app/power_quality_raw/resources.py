from datetime import datetime
from flask_restplus import Resource, reqparse
from marshmallow import ValidationError
from app import db
from . import api
from .models import PowerQualityRaw
from .schemas import PowerQualityRawSchema

SCHEMA = PowerQualityRawSchema()

class PowerQualityRawResource(Resource):
    ''' Power Quality resource for PowerQualityRaw model object '''

    def get(self, object_id=None):
        ''' returns a single MainsPowerRaw object with provided ID or a collection with provided start_time and end_time '''
        if object_id:
            power_item = PowerQualityRaw.query.filter_by(id=object_id).first()
            if not power_item:
                return {'status': 'failure', 'message': 'Item does not exist'}, 404
            data = SCHEMA.dump(power_item)
            return {'status': 'success', 'data': data}, 200
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('start_time', required=True, type=int)
            parser.add_argument('end_time', required=True, type=int)
            parser.add_argument('aggregation', required=True, choices=('none', 'hour', 'day', 'week', 'month', 'year'))
            args = parser.parse_args()

            switch = {
                "none": PowerQualityRaw.timestamp,
                "hour": PowerQualityRaw.get_datetime_by_hour,
                "day": PowerQualityRaw.get_datetime_by_day,
                "week": PowerQualityRaw.get_datetime_by_week,
                "month": PowerQualityRaw.get_datetime_by_month,
                "year": PowerQualityRaw.get_datetime_by_year,
            }

            time_format = switch.get(args['aggregation'])

            start_time = datetime.utcfromtimestamp(args['start_time'])
            end_time = datetime.utcfromtimestamp(args['end_time'])

            power_items = db.session.query(
                db.func.avg(PowerQualityRaw.current_harmonic).label('current_harmonic'), #avg energy in the N harmonic
                db.func.avg(PowerQualityRaw.voltage_harmonic).label('voltage_harmonic'), #avg energy in the N harmonic
                PowerQualityRaw.channel,
                PowerQualityRaw.harmonic_number,
                PowerQualityRaw.additional_current_input,
                time_format.label('aggregate_time'),
            ).filter(PowerQualityRaw.timestamp.between(start_time, end_time)).group_by(time_format, PowerQualityRaw.channel, PowerQualityRaw.harmonic_number, PowerQualityRaw.additional_current_input).order_by(PowerQualityRaw.timestamp).all()
            data = SCHEMA.dump(power_items, many=True)
            return {'status': 'success', 'data': data}, 200

    def post(self):
        ''' creates PowerQualityRaw object and retuns created PowerQualityRaw object '''
        try:
            power = SCHEMA.load(api.payload)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.add(power)

        # Remove oldest record
        row_count = PowerQualityRaw.query.count()
        if row_count > 259200:
            first_poweritem = PowerQualityRaw.query.order_by(PowerQualityRaw.timestamp.asc()).first()
            db.session.delete(first_poweritem)

        db.session.commit()
        data = SCHEMA.dump(power)
        return {'status': 'success', 'data': data}, 201

    def put(self, object_id):
        ''' update and existing PowerQualityRaw object with provided ID '''
        power_item = PowerQualityRaw.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        try:
            SCHEMA.load(api.payload, instance=power_item, session=db.session, partial=True)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204

    def delete(self, object_id):
        ''' deletes a single PowerQualityRaw object '''
        power_item = PowerQualityRaw.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        db.session.delete(power_item)
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204
