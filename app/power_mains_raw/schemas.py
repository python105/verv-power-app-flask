from app import ma
from .models import PowerMainsRaw


class PowerMainsRawSchema(ma.ModelSchema):
    ''' PowerMainsRaw Marshmallow schema Class '''
    class Meta:
        additional = ('aggregate_time',)
        dump_only = ('id', 'timestamp', 'tz_offset', )
        model = PowerMainsRaw
        strict = True
