from flask import Blueprint
from flask_restplus import Api

power_mains_raw = Blueprint('power_mains_raw', __name__)
api = Api(power_mains_raw)

from . import urls

# ERROR HANDLING
@api.errorhandler
def default_error_handler(error):
    return {'status': 'failure', "message": str(error)}
