from . import api
from .resources import PowerMainsRawResource

api.add_resource(PowerMainsRawResource, '/<int:object_id>', '/')
