from datetime import datetime
from flask_restplus import Resource, reqparse
from marshmallow import ValidationError
from app import db
from . import api
from .models import PowerMainsRaw
from .schemas import PowerMainsRawSchema

SCHEMA = PowerMainsRawSchema()

class PowerMainsRawResource(Resource):
    ''' Power resource for PowerMainsRaw model object '''

    def get(self, object_id=None):
        ''' returns a single PowerMainsRaw object with provided ID or a collection with provided start_time and end_time '''
        if object_id:
            power_item = PowerMainsRaw.query.filter_by(id=object_id).first()
            if not power_item:
                return {'status': 'failure', 'message': 'Item does not exist'}, 404
            data = SCHEMA.dump(power_item)
            return {'status': 'success', 'data': data}, 200
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('start_time', required=True, type=int)
            parser.add_argument('end_time', required=True, type=int)
            parser.add_argument('aggregation', required=True, choices=('none', 'hour', 'day', 'week', 'month', 'year'))
            args = parser.parse_args()

            switch = {
                "none": PowerMainsRaw.timestamp,
                "hour": PowerMainsRaw.get_datetime_by_hour,
                "day": PowerMainsRaw.get_datetime_by_day,
                "week": PowerMainsRaw.get_datetime_by_week,
                "month": PowerMainsRaw.get_datetime_by_month,
                "year": PowerMainsRaw.get_datetime_by_year,
            }

            time_format = switch.get(args['aggregation'])

            start_time = datetime.utcfromtimestamp(args['start_time'])
            end_time = datetime.utcfromtimestamp(args['end_time'])

            power_items = db.session.query(
                db.func.avg(PowerMainsRaw.current).label('current'), #avg rms A
                db.func.avg(PowerMainsRaw.voltage).label('voltage'), #avg - rms V
                db.func.avg(PowerMainsRaw.real_power).label('real_power'), #avg v * i W
                db.func.avg(PowerMainsRaw.apparent_power).label('apparent_power'), #avg Vrms * Irms VA
                db.func.avg(PowerMainsRaw.reactive_power).label('reactive_power'), #avg Vrms * Irms VA
                db.func.avg(PowerMainsRaw.power_factor).label('power_factor'), #avg power factor W/VA
                db.func.sum(PowerMainsRaw.cost).label('cost'), #sum of cost
                db.func.sum(PowerMainsRaw.standing_charge).label('standing_charge'), #sum of standing charge
                db.func.sum(PowerMainsRaw.energy).label('energy'), #sum of energy KWh
                db.func.sum(PowerMainsRaw.carbon).label('carbon'), #sum of carbon
                db.func.avg(PowerMainsRaw.frequency).label('frequency'), #avg frequency H
                db.func.avg(PowerMainsRaw.thdc).label('thdc'), #avg Total harmonic distortion current
                db.func.avg(PowerMainsRaw.current_harmonic_1).label('current_harmonic_1'), #avg energy in the 1st harmonic
                db.func.avg(PowerMainsRaw.current_harmonic_2).label('current_harmonic_2'), #avg energy in the 2nd harmonic
                db.func.avg(PowerMainsRaw.current_harmonic_3).label('current_harmonic_3'), #avg energy in the 3rd harmonic
                db.func.avg(PowerMainsRaw.thdv).label('thdv'), #avg Total harmonic distortion voltage
                db.func.avg(PowerMainsRaw.voltage_harmonic_1).label('voltage_harmonic_1'), #avg energy in the 1st harmonic
                db.func.avg(PowerMainsRaw.voltage_harmonic_2).label('voltage_harmonic_2'), #avg energy in the 2nd harmonic
                db.func.avg(PowerMainsRaw.voltage_harmonic_3).label('voltage_harmonic_3'), #avg energy in the 3rd harmonic
                PowerMainsRaw.channel,
                time_format.label('aggregate_time'),
            ).filter(PowerMainsRaw.timestamp.between(start_time, end_time)).group_by(time_format, PowerMainsRaw.channel).order_by(PowerMainsRaw.timestamp).all()
            data = SCHEMA.dump(power_items, many=True)
            return {'status': 'success', 'data': data}, 200

    def post(self):
        ''' creates PowerMainsRaw object and retuns created PowerMainsRaw object '''
        try:
            power = SCHEMA.load(api.payload)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.add(power)

        # Remove oldest record
        row_count = PowerMainsRaw.query.count()
        if row_count > 259200:
            first_poweritem = PowerMainsRaw.query.order_by(PowerMainsRaw.timestamp.asc()).first()
            db.session.delete(first_poweritem)

        db.session.commit()
        data = SCHEMA.dump(power)
        return {'status': 'success', 'data': data}, 201

    def put(self, object_id):
        ''' update and existing PowerMainsRaw object with provided ID '''
        power_item = PowerMainsRaw.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        try:
            SCHEMA.load(api.payload, instance=power_item, session=db.session, partial=True)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204

    def delete(self, object_id):
        ''' deletes a single PowerMainsRaw object '''
        power_item = PowerMainsRaw.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        db.session.delete(power_item)
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204
