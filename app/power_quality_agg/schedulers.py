from datetime import datetime, timedelta
from marshmallow import ValidationError
from apscheduler.schedulers.background import BackgroundScheduler
from app import db
from app.power_quality_raw.models import PowerQualityRaw
from .schemas import PowerQualityAggSchema

SCHEMA = PowerQualityAggSchema()

class PowerQualityAggScheduler(object):
    ''' Scheduler class for aggregating power data from raw table to agg table '''

    def __init__(self):
        self.timer = 15
        self._scheduler = BackgroundScheduler()
        self.job = self._scheduler.add_job(self.aggregate_task, 'cron', minute=('*/%i' % self.timer))
        self._scheduler.start()

    def aggregate_task(self):
        ''' aggregate the data using mathematical functions '''
        end_time = datetime.now()
        start_time = datetime.now() - timedelta(minutes=self.timer)
        power_items = db.session.query(
            db.func.avg(PowerQualityRaw.current_harmonic).label('current_harmonic'), #avg energy in the N harmonic
            db.func.avg(PowerQualityRaw.voltage_harmonic).label('voltage_harmonic'), #avg energy in the N harmonic
            PowerQualityRaw.channel,
            PowerQualityRaw.additional_current_input,
            PowerQualityRaw.harmonic_number,
        ).filter(PowerQualityRaw.timestamp.between(start_time, end_time)).group_by(PowerQualityRaw.channel, PowerQualityRaw.additional_current_input, PowerQualityRaw.harmonic_number).all()
        if power_items:
            for power_item in power_items:
                try:
                    power_item_dict = power_item._asdict()
                    power_item_dict['start_timestamp'] = str(start_time)
                    power_item_dict['end_timestamp'] = str(end_time)
                    power = SCHEMA.load(power_item_dict)
                except ValidationError as err:
                    print(err)
                    return {'status': 'failure', 'message': err.messages}, 400
                db.session.add(power)
            db.session.commit()
            return {'status': 'success'}, 201
