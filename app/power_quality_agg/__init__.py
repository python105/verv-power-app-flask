from flask import Blueprint
from flask_restplus import Api

power_quality_agg = Blueprint('power_quality_agg', __name__)
api = Api(power_quality_agg)

from . import urls
from .schedulers import PowerQualityAggScheduler
PowerQualityAggScheduler()

# ERROR HANDLING
@api.errorhandler
def default_error_handler(error):
    return {'status': 'failure', "message": str(error)}
