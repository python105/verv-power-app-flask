from datetime import datetime
from flask_restplus import Resource, reqparse
from marshmallow import ValidationError
from app import db
from . import api
from .models import PowerQualityAgg
from .schemas import PowerQualityAggSchema

SCHEMA = PowerQualityAggSchema()

class PowerQualityAggResource(Resource):
    ''' Power resource for PowerQualityAgg model object '''

    def get(self, object_id=None):
        ''' returns a single PowerQualityAgg object with provided ID or a collection with provided start_time and end_time '''
        if object_id:
            power_item = PowerQualityAgg.query.filter_by(id=object_id).first()
            if not power_item:
                return {'status': 'failure', 'message': 'Item does not exist'}, 404
            data = SCHEMA.dump(power_item)
            return {'status': 'success', 'data': data}, 200
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('start_time', required=True, type=int)
            parser.add_argument('end_time', required=True, type=int)
            parser.add_argument('aggregation', required=True, choices=('none', 'hour', 'day', 'week', 'month', 'year'))
            args = parser.parse_args()

            switch = {
                "none": PowerQualityAgg.timestamp,
                "hour": PowerQualityAgg.get_datetime_by_hour,
                "day": PowerQualityAgg.get_datetime_by_day,
                "week": PowerQualityAgg.get_datetime_by_week,
                "month": PowerQualityAgg.get_datetime_by_month,
                "year": PowerQualityAgg.get_datetime_by_year,
            }

            time_format = switch.get(args['aggregation'])

            start_time = datetime.utcfromtimestamp(args['start_time'])
            end_time = datetime.utcfromtimestamp(args['end_time'])

            power_items = db.session.query(
                db.func.avg(PowerQualityAgg.current_harmonic).label('current_harmonic'), #avg energy in the N harmonic
                db.func.avg(PowerQualityAgg.voltage_harmonic).label('voltage_harmonic'), #avg energy in the N harmonic
                PowerQualityAgg.channel,
                PowerQualityAgg.harmonic_number,
                PowerQualityAgg.additional_current_input,
                PowerQualityAgg.tz_offset,
                PowerQualityAgg.start_timestamp,
                PowerQualityAgg.end_timestamp,
                time_format.label('aggregate_time'),
            ).filter(PowerQualityAgg.timestamp.between(start_time, end_time)).group_by(time_format, PowerQualityAgg.channel, PowerQualityAgg.harmonic_number, PowerQualityAgg.additional_current_input).order_by(PowerQualityAgg.timestamp).all()

            data = SCHEMA.dump(power_items, many=True)
            return {'status': 'success', 'data': data}, 200

    def post(self):
        ''' creates PowerQualityAgg object and retuns created PowerQualityAgg object '''
        try:
            power = SCHEMA.load(api.payload)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.add(power)
        db.session.commit()
        data = SCHEMA.dump(power)
        return {'status': 'success', 'data': data}, 201

    def put(self, object_id):
        ''' update and existing PowerQualityAgg object with provided ID '''
        power_item = PowerQualityAgg.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        try:
            SCHEMA.load(api.payload, instance=power_item, session=db.session, partial=True)
        except ValidationError as err:
            return {'status': 'failure', 'message': err.messages}, 400
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204

    def delete(self, object_id):
        ''' deletes a single PowerQualityAgg object '''
        power_item = PowerQualityAgg.query.filter_by(id=object_id).first()
        if not power_item:
            return {'status': 'failure', 'message': 'Item does not exist'}, 404
        db.session.delete(power_item)
        db.session.commit()
        data = SCHEMA.dump(power_item)
        return {'status': 'success', 'data': data}, 204
