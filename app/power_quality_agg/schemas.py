from app import ma
from .models import PowerQualityAgg

class PowerQualityAggSchema(ma.ModelSchema):
    ''' PowerQualityAgg Marshmallow schema Class '''
    class Meta:
        additional = ('aggregate_time',)
        dump_only = ('id', 'timestamp', 'tz_offset', )
        model = PowerQualityAgg
        strict = True
