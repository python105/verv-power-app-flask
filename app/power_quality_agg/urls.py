from . import api
from .resources import PowerQualityAggResource

api.add_resource(PowerQualityAggResource, '/<int:object_id>', '/')
