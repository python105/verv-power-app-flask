from datetime import datetime
from sqlalchemy.ext.hybrid import hybrid_property
from app import db

class PowerQualityAgg(db.Model):
    ''' PowerQualityAgg ORM model Class '''
    __tablename__ = 'power_quality_agg'

    id = db.Column(db.Integer, primary_key=True, )
    timestamp = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), unique=True, nullable=False, )
    start_timestamp = db.Column(db.TIMESTAMP, unique=False, nullable=False, )
    end_timestamp = db.Column(db.TIMESTAMP, unique=False, nullable=False, )
    tz_offset = db.Column(db.Integer, default=0, nullable=False, )
    channel = db.Column(db.Integer, nullable=False, )
    harmonic_number = db.Column(db.Integer, nullable=False, )
    current_harmonic = db.Column(db.Float, nullable=False, )
    voltage_harmonic = db.Column(db.Float, nullable=False, )
    additional_current_input = db.Column(db.Boolean, default=False, nullable=False, )

    def __init__(self, **kwargs):
        ''' initialize the PowerQualityAgg model '''
        super(PowerQualityAgg, self).__init__(**kwargs)
        tz_offset = (datetime.now().replace(microsecond=0, second=0)-datetime.utcnow().replace(microsecond=0, second=0)).total_seconds()/3600
        self.timestamp = datetime.now()
        self.tz_offset = tz_offset

    def __repr__(self):
        ''' returns a string of the PowerQualityAgg object '''
        return '<id {}>'.format(self.id)

    @hybrid_property
    def get_datetime_by_hour(self):
        ''' convert timestamp to datetime hourly format '''
        return db.func.strftime('%m/%d/%Y %H:00:00', self.timestamp)

    @hybrid_property
    def get_datetime_by_day(self):
        ''' convert timestamp to datetime daily format '''
        return db.func.strftime('%m/%d/%Y', self.timestamp)

    @hybrid_property
    def get_datetime_by_week(self):
        ''' convert timestamp to datetime weekly format '''
        return db.func.strftime('(%W)%Y', self.timestamp)

    @hybrid_property
    def get_datetime_by_month(self):
        ''' convert timestamp to datetime monthly format '''
        return db.func.strftime('%m/%Y', self.timestamp)

    @hybrid_property
    def get_datetime_by_year(self):
        ''' convert timestamp to datetime yearly format '''
        return db.func.strftime('%Y', self.timestamp)
