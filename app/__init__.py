from flask import Flask, Blueprint, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restplus import Api
from flask_pika import Pika

# INITIALIZATIONS
app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config.ProductionConfig')
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)
pika = Pika(app)

# BLUEPRINTS
from app.power_mains_raw import power_mains_raw
from app.power_mains_agg import power_mains_agg
from app.power_quality_raw import power_quality_raw
from app.power_quality_agg import power_quality_agg
app.register_blueprint(power_mains_raw, url_prefix='/power-mains-raw')
app.register_blueprint(power_mains_agg, url_prefix='/power-mains-agg')
app.register_blueprint(power_quality_raw, url_prefix='/power-quality-raw')
app.register_blueprint(power_quality_agg, url_prefix='/power-quality-agg')

# ERROR HANDLING
@api.errorhandler
def default_error_handler(error):
    return {'status': 'failure', "message": str(error)}
