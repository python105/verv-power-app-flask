import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    ''' Config Constructor Class '''
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'verv-change-this-key'
    
    FLASK_PIKA_PARAMS = {
        'host': 'localhost',
        'username': 'user',
        'password': 'password',
        'port': 5672
    }

    FLASK_PIKA_POOL_PARAMS = {
        'pool_size': 8,
        'pool_recycle': 600
    }

class ProductionConfig(Config):
    ''' Config Production settings Class '''
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.abspath("data/development.sqlite")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class StagingConfig(Config):
    ''' Config Staging settings Class '''
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.abspath("data/development.sqlite")

class DevelopmentConfig(Config):
    ''' Config Development settings Class '''
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.abspath("data/development.sqlite")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class TestingConfig(Config):
    ''' Config Testing settings Class '''
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
