import requests
import pika

HOST = 'rabbitmq'
PORT = 5672
USER = 'user'
PASSWORD = 'password'
QUEUE = 'power.mains.raw'
EXCHANGE = 'power'
TYPE = 'topic'

CREDENTIALS = pika.PlainCredentials(USER, PASSWORD)
CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(host=HOST, port=PORT, credentials=CREDENTIALS))
CHANNEL = CONNECTION.channel()

def callback(channel, method, properties, body):
    ''' callback function for the basic consume rabbitMQ '''
    url = 'http://localhost:8000/power-mains-raw'
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        response = requests.post(url, data=body, headers=headers)
        print(" [x] Received %r" % body)
        print(" [x] Response %r" % response.content)
    except Exception as error:
        print(" [x] Error %r" % error)

CHANNEL.queue_declare(queue=QUEUE)
CHANNEL.exchange_declare(exchange=EXCHANGE, exchange_type=TYPE)
CHANNEL.queue_bind(exchange=EXCHANGE, queue=QUEUE, routing_key=QUEUE)

CHANNEL.basic_consume(QUEUE, callback, auto_ack=True)
print(' [*] Waiting for messages. To exit press CTRL+C')
CHANNEL.start_consuming()
