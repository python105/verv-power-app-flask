import json
import time
import datetime
import random
import pika

HOST = 'rabbitmq'
PORT = 5672
USER = 'user'
PASSWORD = 'password'
POWER_MAINS_QUEUE = 'power.mains.raw'
POWER_QUALITY_QUEUE = 'power.quality.raw'
EXCHANGE = 'power'
TYPE = 'topic'

CREDENTIALS = pika.PlainCredentials(USER, PASSWORD)
CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(host=HOST, port=PORT, credentials=CREDENTIALS))
CHANNEL = CONNECTION.channel()


CHANNEL.exchange_declare(exchange=EXCHANGE, exchange_type=TYPE)
CHANNEL.queue_declare(queue=POWER_MAINS_QUEUE)
CHANNEL.queue_declare(queue=POWER_QUALITY_QUEUE)

while True:

    # Message to the power mains
    for channel in range(1, 4):
        MESSAGE = {
            "daq_timestamp": str(datetime.datetime.now()),
            "current": round(random.uniform(1, 30), 4),
            "voltage": round(random.uniform(220, 240), 4),
            "real_power": round(random.uniform(1, 60), 4),
            "apparent_power": round(random.uniform(1, 60), 4),
            "reactive_power": round(random.uniform(1, 60), 4),
            "cost": round(random.random(), 6),
            "standing_charge": round(random.random(), 6),
            "power_factor": random.randint(1, 10),
            "frequency": random.randint(900, 1000),
            "thdc": round(random.uniform(1, 60), 4),
            "current_harmonic_1": round(random.uniform(1, 60), 4),
            "current_harmonic_2": round(random.uniform(1, 60), 4),
            "current_harmonic_3": round(random.uniform(1, 60), 4),
            "thdv": round(random.uniform(1, 60), 4),
            "voltage_harmonic_1": round(random.uniform(1, 60), 4),
            "voltage_harmonic_2": round(random.uniform(1, 60), 4),
            "voltage_harmonic_3": round(random.uniform(1, 60), 4),
            "energy": round(random.uniform(1, 60), 4),
            "carbon": round(random.uniform(1, 60), 4),
            "channel": channel
        }
        try:
            CHANNEL.basic_publish(exchange=EXCHANGE, routing_key=POWER_MAINS_QUEUE, body=json.dumps(MESSAGE))
            print("[x] Sent %s" % (json.dumps(MESSAGE)))
        except Exception as error:
            print("[x] Failed %s" % (error))

    # Message to the power quality
    for harmonic_number in range(1, 51):
        for channel in range(1, 4):
            MESSAGE = {
                "harmonic_number": harmonic_number,
                "voltage_harmonic": round(random.uniform(220, 240), 4),
                "current_harmonic": round(random.uniform(1, 30), 4),
                "daq_timestamp": str(datetime.datetime.now()),
                "additional_current_input": random.choice(["true", "false"]),
                "channel": channel
            }
            try:
                CHANNEL.basic_publish(exchange=EXCHANGE, routing_key=POWER_QUALITY_QUEUE, body=json.dumps(MESSAGE))
                print("[x] Sent %s" % (json.dumps(MESSAGE)))
            except Exception as error:
                print("[x] Failed %s" % (error))

    # Sleep between messages
    time.sleep(1)

CONNECTION.close()
