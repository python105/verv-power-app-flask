FROM python:3.8.0b4-alpine3.10
#FROM arm32v7/python:3.8.0b4-alpine3.10
#FROM arm64v8/python:3.8.0b4-alpine3.10
LABEL maintainer="Dor Cohen"
EXPOSE 8000
ENV TZ Etc/UTC
WORKDIR /usr/src/app
COPY . ./
RUN apk --update add python py-pip openssl ca-certificates py-openssl wget mariadb-dev gfortran libpng-dev openblas-dev supervisor
RUN apk --update add --virtual build-dependencies libffi-dev openssl-dev python-dev py-pip build-base gcc musl-dev openblas-dev freetype-dev pkgconfig \
  && pip3 install --upgrade pip \
  && pip3 install -r requirements.txt \
  && apk del build-dependencies
RUN mkdir -p /var/log/supervisors
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN python3 manage.py db init
RUN python3 manage.py db migrate
RUN python3 manage.py db upgrade
#CMD exec gunicorn app:app --log-level debug --bind 0.0.0.0:8000
CMD supervisord --nodaemon --configuration /etc/supervisor/conf.d/supervisord.conf