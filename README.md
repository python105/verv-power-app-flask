# Description
This APP is a database microservice wrapped in flask to provide API access for power data

![image not available](gunicorn.png "Gunicorn flask deployment")


# Build the development environment

Download and install VS-CODE:

<a>https://code.visualstudio.com/download</a>

Download and install POSTMAN:

<a>https://www.getpostman.com/downloads/</a>

Import the postman-collection into postman:

`
postman->import->EVO.postman_collection.json
`

Install the following extensions for VS-CODE 

(`VS-CODE->LEFT PANEL->EXTENSIONS`):
- Docker
- GIT History
- GitLens
- Python
- SQLTools - Database tools
- SQLite
- Partial Diff

Restart VS-CODE

Create python virtual environment and install pip requirements:
```bash
python3 -m venv venv
. venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt
```

Create a linting file:
```yaml
pylint --generate-rcfile > pylintrc 
```

add the following at the `disable` statement in the linting file:
```yaml
disable=missing-docstring
```

add the following at the `ignored-modules` statement in the linting file:
```yaml
ignored-modules=flask_sqlalchemy
```

Edit the `app/__init__.py` file to development environment:
```python
app.config.from_object('config.DevelopmentConfig')
```

Run the following to make database migrations (create database tables):

```bash
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
```

Run the following to start the server:
```bash
python manage.py runserver -p 8000 -h 0.0.0.0
```

# Build for a local PC

Edit the dockerfile add comment the right image for ARMV7:
```python
FROM python:3.8.0b4-alpine3.10
#FROM arm32v7/python:3.8.0b4-alpine3.10
#FROM arm64v8/python:3.8.0b4-alpine3.10
```

Edit the `app/__init__.py` file to production environment:
```python
app.config.from_object('config.ProductionConfig')
```

Run the following to build the docker image:
```bash
docker build --no-cache -t power_database_pc .
```

Run the following to create the container:
```bash
sudo docker run -d --name power_database_pc -p 8000:8000 -it power_database_pc
```

Run the following to make sure the container is running:
```bash
sudo docker ps -a
```

Run the following to kill the running container:
```bash
sudo docker rm -f power_database_pc
```

# Build for raspberry PI

Edit the dockerfile add comment the right image for ARMV7:
```python
#FROM python:3.8.0b4-alpine3.10
FROM arm32v7/python:3.8.0b4-alpine3.10
#FROM arm64v8/python:3.8.0b4-alpine3.10
```

Edit the `app/__init__.py` file to production environment:
```python
app.config.from_object('config.ProductionConfig')
```

Run the following to build the docker container:
```bash
docker build --no-cache -t power_database_pi .
```

Run the following to export the image to a raspberry Pi:
```bash
docker save power_database_pi > power_database_pi.tar
scp power_database_pi.tar  <PI USER NAME>@<PI IP ADDRESS>:/home/<PI USER NAME>
```

SSH to the PI and load the image:
```bash
ssh <PI USER NAME>@<PI IP ADDRESS>
docker load < power_database_pi.tar
```

Make sure the image is loaded using:
```bash
sudo docker images
```

Run the following to create the container:
```bash
sudo docker run -d --name power_database_pi -p 8000:8000 -it power_database_pi
```

Run the following to make sure the container is running:
```bash
sudo docker ps -a
```

Run the following to access the container and debug it:
```bash
sudo docker logs power_database_pi
sudo docker exec -it power_database_pi /bin/sh
```

Run the following to kill the running container:
```bash
sudo docker rm -f power_database_pi
```

# Volumes
For production deploeyment a volume needs to be added and attached to the docker container.

This will prevent a loss of data upon container restart.

Attach the volume to the container using the following command:
```bash
docker run -d -v <path to folder>/data_volume:/usr/src/app/data
```
The mount is created inside the container’s `/usr/src/app/data` directory. 

# RabbitMQ

Create a RabbitMQ network using:
```bash
docker network create RabbitMQ
```

Create a RabbitMQ container using:
```bash
 docker run -d --hostname rabbitmq --name rabbitmq -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password -p 15672:15672 -p 5672:5672  rabbitmq:3-management
 ```

# Docker-Compose
```bash
docker-compose build --no-cache
docker-compose up
```
